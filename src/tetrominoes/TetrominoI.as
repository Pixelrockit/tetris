package tetrominoes 
{
	import starling.display.DisplayObject;

	public final class TetrominoI extends DisplayObject
	{
		private static const ROTATION_1:Vector.<Vector.<int>> = new <Vector.<int>>
		[
			new <int>[1, 1, 1, 1],
		]
		
		private static const ROTATION_2:Vector.<Vector.<int>> = new <Vector.<int>>
		[
			new<int>[1],
			new<int>[1],
			new<int>[1],
			new<int>[1]
		]
		
		private static const ROTATION_3:Vector.<Vector.<int>> = new <Vector.<int>>
		[

			new<int>[1, 1, 1, 1],

		]
		
		private static const ROTATION_4:Vector.<Vector.<int>> = new <Vector.<int>>
		[
			new<int>[1],
			new<int>[1],
			new<int>[1],
			new<int>[1]
		]
		
		public static const ROTATIONS:Vector.<Vector.<Vector.<int>>> = new <Vector.<Vector.<int>>>
		[
			ROTATION_1, 
			ROTATION_2, 
			ROTATION_3, 
			ROTATION_4
		]
		
		public static const INITIAL_OFFSET:int = 3;
		public static const TEXTURE:String = "greenLite";
	}
}