package textures 
{
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public final class Textures
	{
		[Embed(source="../../assets/tetris_main_001.xml", mimeType="application/octet-stream")]
		private static const tetris_main_001_XML:Class; 
		
		[Embed(source="../../assets/tetris_main_001.png")]
		private static const tetris_main_001:Class; //class name needs to be identical with file name - required by Starling
		
		private static var _primaryTexture:Texture;
		private static var _primaryTextureXML:XML;
		private static var _primaryTextureAtlas:TextureAtlas;
		
		public static var backgroundImage:Texture;
		public static var tiles:Object;
		public static var gridStencil:Texture;
		public static var mainPanel:Texture;
		public static var shortPanel:Texture;
		public static var iconNext:Texture;
		
		public static function init():void
		{
			_primaryTexture = Texture.fromBitmap(new tetris_main_001(), false); //we do not need to generate mipmaps - there will be no scaling
			_primaryTextureXML = XML(new tetris_main_001_XML());
			_primaryTextureAtlas = new TextureAtlas(_primaryTexture, _primaryTextureXML);
			
			backgroundImage = _primaryTextureAtlas.getTexture("bg_main");
			
			tiles = 
			{
				greenDark: 	_primaryTextureAtlas.getTexture("game_object_tile_green_dark"),
				greenLite:	_primaryTextureAtlas.getTexture("game_object_tile_green_light"),
				magenta: 	_primaryTextureAtlas.getTexture("game_object_tile_magenta"),
				orange: 	_primaryTextureAtlas.getTexture("game_object_tile_orange"),
				red: 		_primaryTextureAtlas.getTexture("game_object_tile_red"),
				violet: 	_primaryTextureAtlas.getTexture("game_object_tile_violet"),
				yellow: 	_primaryTextureAtlas.getTexture("game_object_tile_yellow")
			}
			
			//stencil is not used to create tetrominoes, thats why it is not a part of the tiles object
			gridStencil = _primaryTextureAtlas.getTexture("game_object_tile_stencil");
			mainPanel = _primaryTextureAtlas.getTexture("ui_panel_main");
			shortPanel = _primaryTextureAtlas.getTexture("ui_panel_short");
			iconNext = _primaryTextureAtlas.getTexture("ui_icon_tile_next");
			
			trace(backgroundImage);
		}
		
		public function Textures()
		{
			throw new Error("This is a static class. Call init() to initialize assets");
		}
	}
}