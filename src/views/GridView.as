package views
{	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.textures.Texture;
	
	import textures.Textures;
	
	public final class GridView extends Sprite
	{
		private var _gridTexture:Texture;
		private var _gridTextureWidth:Number
		private var _gridTextureHeight:Number
		
		private var _model:Object;
		private var _controller:Object;
		
		//_modelGrid and _visualGrid are mapped 1:1
		//_modelGrid is an abstarct representation of the gameplay grid
		//_visualGrid is a visual representation of gameplay grid
		private var _modelGrid:Vector.<Vector.<int>>;
		private var _visualGrid:Vector.<Vector.<Image>>;
		
		private var _differentialGrid:Vector.<Vector.<int>>;
		
		public function GridView(model:Object, controller:Object)
		{
			super();
			
			_model = model;
			_controller = controller;
			_model.addEventListener(Event.CHANGE, onUpdate);
			
			_modelGrid = _model.currentGrid;
			_differentialGrid = _model.differentialGrid;
			
			_gridTexture = _model.BASIC_TEXTURE;
			_gridTextureWidth = _gridTexture.width;
			_gridTextureHeight = _gridTexture.height;
			
			_visualGrid = initializeVisualGrid();
			_visualGrid.fixed = true;
			
			addVisualGridToStage();
			
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			_controller.processKeyDown(event);
		}
		
		private function onKeyUp(event:KeyboardEvent):void
		{
			_controller.processKeyUp(event);
		}
		
		//fills visualGrid vector with Vector.<Image> instances
		private function initializeVisualGrid():Vector.<Vector.<Image>>
		{
			var grd:Vector.<Vector.<Image>> = new Vector.<Vector.<Image>>(_modelGrid.length, true);
			var row:Vector.<Image>;
			
			for (var i:int = 0; i < _modelGrid.length; i++) 
			{
				row = new Vector.<Image>(_modelGrid[i].length, true);
				
				for (var j:int = 0; j < row.length; j++) 
				{
					row[j] = new Image(_model.BASIC_TEXTURE);
				}
				
				grd[i] = row;
			}
			
			return grd;
		}
		
		//adds the grid to stage
		private function addVisualGridToStage():void
		{
			var gridPiece:Image
			for (var i:int = 0; i < _visualGrid.length; i++) 
			{
				for (var j:int = 0; j < _visualGrid[i].length; j++) 
				{
					gridPiece = _visualGrid[i][j];
					gridPiece.x = (_model.TEXTURE_WIDTH + _model.SPACER) * j;
					gridPiece.y = (_model.TEXTURE_WIDTH + _model.SPACER) * i;
					this.addChild(gridPiece);
				}
				
			}
			
		}
		
		//refreshes the gid on each update, refreshes only the differences since last movement
		private function onUpdate(event:Event):void
		{
			var row:int;
			var column:int;		
			var value:int;	
			
			for (var i:int = 0; i < _model.differentialGrid.length; i++) 
			{
				row = _model.differentialGrid[i][0];
				column = _model.differentialGrid[i][1];
				value = _model.differentialGrid[i][2];
				_visualGrid[row][column].texture = (value == 0) ? _model.BASIC_TEXTURE : Textures.tiles[_model.currentTexture];
			}
		}
	}
}