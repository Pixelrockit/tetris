package views
{
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import textures.Textures;
	
	public class NextView extends Sprite
	{
		private var _model:Object;
		private var _panel:Image;
		private var _icon:Image;
		
		private var _modelGrid:Vector.<Vector.<int>>;
		private var _visualGrid:Vector.<Vector.<Image>>;
		
		public function NextView(model:Object)
		{
			super();
			_model = model;
			_model.addEventListener("TETROMINO_SELECTED", update);
			
			_panel = new Image(Textures.shortPanel);
			this.addChild(_panel);
			
			_icon = new Image(Textures.iconNext);
			_icon.pivotX = _icon.width / 2;
			_icon.pivotY = _icon.height / 2;
			_icon.y = _panel.height / 2;
			_icon.x = 30;
			this.addChild(_icon);
			
			_modelGrid = _model.nextViewGrid;
			
			
			_visualGrid = initializeVisualGrid();
			_visualGrid.fixed = true;
			
			addVisualGridToStage();
						
		}
		
		private function initializeVisualGrid():Vector.<Vector.<Image>>
		{
			var grd:Vector.<Vector.<Image>> = new Vector.<Vector.<Image>>(_modelGrid.length, true);
			var row:Vector.<Image>;
			
			for (var i:int = 0; i < _modelGrid.length; i++) 
			{
				trace(_modelGrid[i]);

				row = new Vector.<Image>(_modelGrid[i].length, true);
				
				for (var j:int = 0; j < row.length; j++)
				{
					row[j] = new Image(_model.BASIC_TEXTURE);
				}
				
				grd[i] = row;
			}
			
			return grd;
		}
		
		private function addVisualGridToStage():void
		{
			var gridPiece:Image
			for (var i:int = 0; i < _visualGrid.length; i++) 
			{
				for (var j:int = 0; j < _visualGrid[i].length; j++) 
				{
					gridPiece = _visualGrid[i][j];
					gridPiece.x = (_model.TEXTURE_WIDTH + _model.SPACER) * j + 65;
					gridPiece.y = (_model.TEXTURE_WIDTH + _model.SPACER) * i + 15;
					this.addChild(gridPiece);
				}
				
			}
			
		}
		
		private function update(event:Event):void
		{
			var row:Vector.<int>;
			var nextTetrRow:Vector.<int>;
			var column:int;
			var value:int;
			
			resetGrid();
			
			for (var i:int = 0; i < _model.nextTetromino.length; i++) 
			{
				row = _model.nextTetromino[i];
				for (var j:int = 0; j < row.length; j++) 
				{
					value = _model.nextTetromino[i][j];
					_visualGrid[i][j].texture = (value == 0) ? _model.BASIC_TEXTURE : Textures.tiles[_model.nextTexture];
				}
				
			}
			
			
			
			
		}
		
		private function resetGrid():void
		{
			for (var i:int = 0; i < _visualGrid.length; i++) 
			{
				for (var j:int = 0; j < _visualGrid[i].length; j++) 
				{
					trace("resetting grid");
					_visualGrid[i][j].texture = _model.BASIC_TEXTURE;
				}
				
			}
		}
	}
}