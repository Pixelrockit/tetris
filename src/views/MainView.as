package views
{
	import starling.display.BlendMode;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.Sprite;
	import textures.Textures;
		
	public class MainView extends Sprite
	{
		private var _background:Image;
		private var _mainPanel:Image;
		
		private var _gridView:Sprite;
		
		private var _mainModel:Object;
		private var _mainController:Object;
		
		private var _currentTetromino:DisplayObjectContainer;
		private const _BOTTOM_OFFSET:Number = 10;

		/**
		 *Just a static view without a model or controller
		 * it just functions as a display object container for the background and other views 
		 * 
		 */		
		public function MainView()
		{
			super();
			//nothing more to do here
		}
		
		public function start():void
		{
			//adding graphics to the main view
			
			_background = new Image(Textures.backgroundImage);
			
			//this image doesn't need transparency, this adds one more draw call but saves cpu time
			_background.blendMode = BlendMode.NONE; 
			this.addChild(_background);
			
			_mainPanel = new Image(Textures.mainPanel);
			_mainPanel.pivotX = _mainPanel.width / 2;
			_mainPanel.pivotY = _mainPanel.height / 2;
			_mainPanel.x = stage.width / 2;
			_mainPanel.y = stage.height / 2 - _BOTTOM_OFFSET;
			this.addChild(_mainPanel);
		}
	}
}