package views
{
	import starling.events.Event;
	import starling.text.TextField;
	
	public final class PointsView extends TextField
	{
		private var _model:Object;
		
		public function PointsView(model:Object)
		{
			var width:Number = 200;
			var height:Number = 60;
			var text:String = "0000";
			super(width, height, text);
			
			_model = model;
			_model.addEventListener("INCREASE_POINTS", update);
		}
		
		private function update(event:Event):void
		{
			this.text = event.data.toString();
		}
	}
}