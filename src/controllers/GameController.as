package controllers
{
	
	import starling.events.KeyboardEvent;

	public class GameController
	{
		private var _model:Object;
		
		public function GameController(model:Object)
		{
			_model = model;	
		}
		
		public function processKeyDown(event:KeyboardEvent):void
		{
			_model.respondToUserInput(event.keyCode);
		}
		
		public function processKeyUp(event:KeyboardEvent):void
		{
			_model.slowDownTimer(event.keyCode);
		}
	}
}