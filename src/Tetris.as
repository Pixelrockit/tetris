package
{
	import flash.display.Sprite;
	
	import controllers.GameController;
	
	import models.GameModel;
	
	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import textures.Textures;
	
	import views.GridView;
	import views.MainView;
	import views.NextView;
	import views.PointsView;
	
	[SWF(width='760', height='600', frameRate='60')]
	
	/**
	 *@author Bartosz Oczujda 
	 */	
	public class Tetris extends Sprite
	{
		private var _starlingInstance:Starling;
		private var _mainModel:Object;
		private var _mainController:Object;
		private var _pointsView:TextField;
		private var _nextView:DisplayObjectContainer;
		
		private var _gameModel:Object;
		private var _gridView:DisplayObjectContainer;
		private const _BOTTOM_OFFSET:Number = 10;
		
		public function Tetris()
		{
			_starlingInstance = new Starling(MainView, stage);
			_starlingInstance.showStats = true;
			_starlingInstance.start();
			
			_starlingInstance.addEventListener(Event.ROOT_CREATED, onRootCreated);
		}
		
		private function onRootCreated(event:Event, mainView:MainView):void
		{
			trace("Begin");
			trace("Now changing master");
			//need to init this after ROOT_CREATED, else starling context is missing
			Textures.init();
			
			_gameModel = new GameModel();
			_mainController = new GameController(_gameModel);
			_gridView = new GridView(_gameModel, _mainController);
			
			//Assets are initiated, game can start
			mainView.start();
			
			//adding all views to the stage and adjusting their visual properties
			_gridView.x = mainView.width / 2 - _gridView.width / 2;
			_gridView.y = mainView.height / 2 - _gridView.height / 2 - _BOTTOM_OFFSET;
			mainView.addChild(_gridView);
			
			_pointsView = new PointsView(_gameModel);
			_pointsView.hAlign = HAlign.RIGHT;
			_pointsView.vAlign = VAlign.TOP;
			_pointsView.fontSize = 50;
			_pointsView.autoSize = TextFieldAutoSize.HORIZONTAL;
			_pointsView.x = 550;
			_pointsView.y = 20;
			_pointsView.color = 0xFFC000;
			_pointsView.bold = true;
			mainView.addChild(_pointsView);
			
			_nextView = new NextView(_gameModel);
			_nextView.x = 40;
			_nextView.y = 100;
			mainView.addChild(_nextView);
			trace("GAME STARTED+++");
		}
	}
}