package models
{
	
	import flash.ui.Keyboard;
	
	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.textures.Texture;
	
	import tetrominoes.TetrominoI;
	import tetrominoes.TetrominoL;
	import tetrominoes.TetrominoLMirrored;
	import tetrominoes.TetrominoN;
	import tetrominoes.TetrominoNMirrored;
	import tetrominoes.TetrominoSquare;
	import tetrominoes.TetrominoT;
	
	import textures.Textures;
	
	public class GameModel extends EventDispatcher
	{
		//constants for the ain grid
		public const NUM_OF_COLUMNS:int = 10;
		public const NUM_OF_ROWS:int = 20;
		public const TEXTURE_WIDTH:Number = 24;
		public const SPACER:Number = 2;
		
		//constants for next view grid
		public const NEXT_NUM_OF_COLUMNS:int = 4;
		public const NEXT_NUM_OF_ROWS:int = 2;
		
		public const BASIC_TEXTURE:Texture = Textures.gridStencil;
		public var currentTexture:String;
		public var nextTexture:String;
		private var _beginColumn:int = 0;
		private var _beginRow:int = 0;
		private var _currentColumn:int;
		private var _currentRow:int;
		public var spacesFilled:Vector.<Vector.<int>>;
		
		public var currentGrid:Vector.<Vector.<int>> = new Vector.<Vector.<int>>(NUM_OF_ROWS, true);
		public var oldGrid:Vector.<Vector.<int>> = new Vector.<Vector.<int>>(NUM_OF_ROWS, true);
		public var differentialGrid:Vector.<Vector.<int>>;
		public var nextViewGrid:Vector.<Vector.<int>> = new Vector.<Vector.<int>>(NEXT_NUM_OF_ROWS, true);
		private var _currentTetromino:Vector.<Vector.<int>>;
		public var nextTetromino:Vector.<Vector.<int>>;
		private var _tetrominoHeight:int;
		private var _tetrominoWidth:int;
		private var _isTetrominoPresent:Boolean = false;
		
		private const _NORMAL_TIME:Number = 1;
		private const _FASTER_TIME:Number = 0.1;
		
		private var _isGameOver:Boolean = false;
		
		public var points:int = 0;
		
		private var shapeCollection:Vector.<Object> = new <Object>
			[
				TetrominoI,
				TetrominoL,
				TetrominoLMirrored,
				TetrominoN,
				TetrominoNMirrored,
				TetrominoSquare,
				TetrominoT
			]
		
		private var timer:DelayedCall;
		private var _spacePressed:Boolean = false;
		
		private var currentTetrominoNum:Number;
		private var nextTetrominoNum:Number;
		private var currentRotation:int;
		
		public function GameModel()
		{
			currentTetrominoNum = Math.floor(Math.random() * (shapeCollection.length));
			createMainGrid();			
			createMiniGrid();			
			
			/*
			* this timer controls the whole game
			* on each tick it updates the vector containing the game grid
			* and dispatches an event to update the view
			* no need for enterframe event and because it is invoked by Starling juggler
			* it stays synchronised with the framerate
			*/
			timer = Starling.juggler.delayCall(update, _NORMAL_TIME, false);
			timer.repeatCount = 0;
		}
		
	
		//create 2 dimensional vector representing the game grid
		private function createMainGrid():void
		{			
			for (var i:int = 0; i < NUM_OF_ROWS; i++)
			{
				currentGrid[i] = new Vector.<int>
				
				for (var j:int = 0; j < NUM_OF_COLUMNS; j++)
				{
					currentGrid[i][j] = 0;
				}
			}
		}
		
		//create 2 dimensional vector representing the grid in next tetromino view
		private function createMiniGrid():void
		{
			for (var i:int = 0; i < NEXT_NUM_OF_ROWS; i++)
			{
				nextViewGrid[i] = new Vector.<int>
				
				for (var j:int = 0; j < NEXT_NUM_OF_COLUMNS; j++)
				{
					nextViewGrid[i][j] = 0;
				}
			}
		}
		
		
		//add tetromino to the grid
		private function initTetromino():void
		{			
			
			currentRotation = 0;

			nextTetrominoNum = Math.floor(Math.random() * (shapeCollection.length));
			nextTetromino = shapeCollection[nextTetrominoNum].ROTATIONS[currentRotation];
			nextTexture = shapeCollection[nextTetrominoNum].TEXTURE;
			dispatchEventWith("TETROMINO_SELECTED", false, points);

			_currentTetromino = shapeCollection[currentTetrominoNum].ROTATIONS[currentRotation];
			currentTexture = shapeCollection[currentTetrominoNum].TEXTURE;
			_currentColumn = shapeCollection[currentTetrominoNum].INITIAL_OFFSET;
			_currentRow = _beginRow;
			fillSpaces();
		}
		
		//fill spaces in the model grid
		private function fillSpaces():void
		{
			var currTetrRow:Vector.<int>;
			var tetrWidth:int;
			var oldTetrWidth:int;
			spacesFilled = new Vector.<Vector.<int>>;
			var currTetrColumn:int;
			
			var currTetLength:uint = _currentTetromino.length
			for (var i:int = 0; i < currTetLength; i++) 
			{
				currTetrRow = _currentTetromino[i];
				
				//omitting empty rows in tetromino
				if(currTetrRow.indexOf(1) > -1)
				{
					tetrWidth = currTetrRow.length;
					//if line is not empty we fill proper positions with with 2
					for (var j:int = 0; j < currTetrRow.length; j++) 
					{	
						if(currTetrRow[j] > 0) 
						{
							//tetrWidth++;
							currTetrColumn++;
							
							spacesFilled.push(new <int>[_currentRow + i, _currentColumn + j]);
							
							//check for game over - if the new tetromino overlaps a filled space on the grid
							if (_currentRow == 0 && isGameOver())
							{
								timer.reset(update, 0);
								trace("timer stopped");
							}
							
							currentGrid[_currentRow + i][_currentColumn + j] = 2;
							
							if(tetrWidth > oldTetrWidth) oldTetrWidth = tetrWidth;
							
						}							
					}					
				}				
			}
			
			_tetrominoHeight = _currentTetromino.length;
			_tetrominoWidth = oldTetrWidth;
		}		
		
		private function isGameOver():Boolean
		{
			var cell:Vector.<int>;
			var row:int;
			var column:int;
			
			for (var i:int = 0; i < spacesFilled.length; i++) 
			{
				cell = spacesFilled[i];
				row = cell[0];
				column = cell[1];
				
				if(currentGrid[row][column] == 1)
				{
					_isGameOver = true;
					return true;
				}
				
			}
			
			return false;
		}
		
		private function moveTetrominoDown():void
		{
			var cell:Vector.<int>;
			var row:int;
			var column:int;
			
			//check for collision with standing bricks
			for (var i:int = 0; i < spacesFilled.length; i++) 
			{
				cell = spacesFilled[i];
				row = cell[0];
				column = cell[1];
				
				if(row + 1 < NUM_OF_ROWS && currentGrid[row + 1][column] == 1)
				{
					freezeTetromino();
					_isTetrominoPresent = false;
					return;
				}
			}
			
			//check for collision with bottom of the grid
			if(_currentRow + _tetrominoHeight < NUM_OF_ROWS + 1)
			{
				resetTetromino();
				fillSpaces();
			}
			else
			{
				freezeTetromino();
				_isTetrominoPresent = false;
				return;
			}	
			
			
			
		}
		
		public function respondToUserInput(keyCode:uint):void
		{
			var cell:Vector.<int>;
			var row:int;
			var column:int;
			
			if(keyCode == Keyboard.LEFT && _currentColumn > 0)
			{
				//check for collision with bricks on the left
				for (var i:int = 0; i < spacesFilled.length; i++) 
				{
					cell = spacesFilled[i];
					row = cell[0];
					column = cell[1];
					
					if(column - 1 > 0 && currentGrid[row][column - 1] == 1)
					{
						return;
					}
				}
				
				_currentColumn -=1;
				oldGrid = cloneCurrentGrid();
				resetTetromino();
				fillSpaces();
				update(true);
			}
			else if(keyCode == Keyboard.RIGHT && _currentColumn + _tetrominoWidth < NUM_OF_COLUMNS)
			{
				//check for collision with bricks on the right
				for (var k:int = 0; k < spacesFilled.length; k++) 
				{
					cell = spacesFilled[k];
					row = cell[0];
					column = cell[1];
					
					if(column + 1 < NUM_OF_COLUMNS && currentGrid[row][column + 1] == 1)
					{
						return;
					}
				}
				
				_currentColumn +=1;
				oldGrid = cloneCurrentGrid();
				resetTetromino();
				fillSpaces();
				update(true);
			}
			
			if(keyCode == Keyboard.DOWN) rotateTetromino(false);
			else if(keyCode == Keyboard.UP) rotateTetromino(true);
			
			if(keyCode == Keyboard.SPACE) speedUpTimer();

		}
		
		private function rotateTetromino(rotateUp:Boolean):void
		{
			if(rotateUp && isRotationPossible(true))
			{
				currentRotation++;
				if(currentRotation > shapeCollection[currentTetrominoNum].ROTATIONS.length - 1) currentRotation = 0;
				_currentTetromino = shapeCollection[currentTetrominoNum].ROTATIONS[currentRotation];
				oldGrid = cloneCurrentGrid();
				resetTetromino();
				fillSpaces();
				update(true);
			}
			else if(!rotateUp && isRotationPossible(false))
			{
				currentRotation--;
				if(currentRotation < 0) currentRotation = shapeCollection[currentTetrominoNum].ROTATIONS.length - 1;
				_currentTetromino = shapeCollection[currentTetrominoNum].ROTATIONS[currentRotation];
				oldGrid = cloneCurrentGrid();
				resetTetromino();
				fillSpaces();
				update(true);
			}
		}
		
		private function isRotationPossible(isUp:Boolean):Boolean
		{
			var newRotation:int = isUp ? currentRotation+1 : currentRotation-1;
			var newSpacesFilled:Vector.<Vector.<int>> = new Vector.<Vector.<int>>;
			
			newRotation = newRotation < 0 ? shapeCollection[currentTetrominoNum].ROTATIONS.length - 1 : newRotation;
			newRotation = newRotation > shapeCollection[currentTetrominoNum].ROTATIONS.length - 1 ? 0 : newRotation;
			
			var newShape:Vector.<Vector.<int>> = shapeCollection[currentTetrominoNum].ROTATIONS[newRotation];
			
			var row:Vector.<int>;
			for (var i:int = 0; i < newShape.length; i++) 
			{
				row = newShape[i];
				
				for (var j:int = 0; j < row.length; j++) 
				{
					if(row[j] == 1) newSpacesFilled.push(new <int>[_currentRow + i, _currentColumn + j]);
				}
				
			}
			
			
			
			//checking for tetromino being out of the grid
			for (var i2:int = 0; i2 < newSpacesFilled.length; i2++) 
			{
				if(newSpacesFilled[i2][1] > NUM_OF_COLUMNS - 1) return false;
				if(newSpacesFilled[i2][0] > NUM_OF_ROWS - 1) return false;
			}
			
			var cell:Vector.<int>;
			var row2:int;
			var column:int;
			
			//checking for collision with static bricks
			for (var k:int = 0; k < newSpacesFilled.length; k++) 
			{
				cell = newSpacesFilled[k];
				row2 = cell[0];
				column = cell[1];
				
				if(currentGrid[row2][column] == 1)
				{
					return false
				}
				
			}
			
			
			
			
			return true;
		}
		
		//speeds up the falling of tetrominoes
		private function speedUpTimer():void
		{
			if (_spacePressed) return;
			
			timer.reset(update, _FASTER_TIME, [false]);
			timer.repeatCount = 0;
			_spacePressed = true;
		}
		
		//slows down the falling of tetrominoes
		public function slowDownTimer(keyCode:uint):void
		{
			if(keyCode != Keyboard.SPACE) return;
			
			timer.reset(update, _NORMAL_TIME, [false]);
			timer.repeatCount = 0;
			_spacePressed = false;
		}
		
		//stops the teromino in place in case of a collision
		private function freezeTetromino():void
		{
			var cell:Vector.<int>;
			var row:int;
			var column:int;
			
			for (var i:int = 0; i < spacesFilled.length; i++) 
			{
				cell = spacesFilled[i];
				row = cell[0];
				column = cell[1];
				currentGrid[row][column] = 1;
			}
			
			currentTetrominoNum = nextTetrominoNum;
			dispatchEventWith("TETROMINO_SELECTED", false, points);

		}
		
		//restets the old tetromino position
		private function resetTetromino():void
		{
			var row:int;
			var column:int;
			for (var i:int = 0; i < spacesFilled.length; i++) 
			{
				row = spacesFilled[i][0];
				column = spacesFilled[i][1];
				
				currentGrid[row][column] = 0;
			}
		}
		
		//updates the model and dispatches the change event on each timer step or on keyboard event
		private function update(userInitiated:Boolean):void
		{
			
			if(_isGameOver) return;
			
			if(!userInitiated)
			{
				if (!_isTetrominoPresent) 
				{
					oldGrid = cloneCurrentGrid();
					checkForFilledLines();
					
					initTetromino();
					
					resetTetromino();
					fillSpaces();
					_isTetrominoPresent = true;
				}
				else
				{
					oldGrid = cloneCurrentGrid();
					_currentRow++;
					moveTetrominoDown();
					
				}
			}
			
			
			
			differentialGrid = calculateGridDifference();
			
			dispatchEventWith(Event.CHANGE);
		}
		
		//checks for lines wich are full and marks them for deletion
		private function checkForFilledLines():void
		{
			var numFilled:int
			
			for (var i:int = 0; i < currentGrid.length; i++) 
			{
				numFilled = 0;
				for (var j:int = 0; j < currentGrid[i].length; j++) 
				{
					if(currentGrid[i][j] != 1) break;
					numFilled++;
				}
				
				if(numFilled == NUM_OF_COLUMNS) 
				{
					points+=1000;
					deleteLine(i);
				}
				
			}
			
		}		
		
		//takes care of line deletion and moves the rest of the blocks down
		private function deleteLine(index:int):void
		{			
			// Moves all the upper lines one row down
			for (var i:int = index; i > 0; i--) 
			{
				for (var j:int = 0; j < NUM_OF_COLUMNS; j++)
				{
					currentGrid[i][j] = currentGrid[i-1][j];
					dispatchEventWith("INCREASE_POINTS", false, points);
				}
				
			}
			
		}
		
		//returns vector containing the difference between the new state of the grid
		//and previous state of the grid - used for updating the view,
		//only places that changed since last update are updated
		private function calculateGridDifference():Vector.<Vector.<int>>
		{
			
			var tempVector:Vector.<Vector.<int>> = new Vector.<Vector.<int>>;
			
			for (var i:int = 0; i < currentGrid.length; i++) 
			{
				for (var j:int = 0; j < currentGrid[i].length; j++) 
				{					
					//if the brick is frozen now and it was empty, it should be frozen
					if(currentGrid[i][j] == 1 && oldGrid[i][j] == 0) tempVector.push(new <int>[i,j, 1]);
					//if the brick is empty now and it was frozen, it should be empty
					if(currentGrid[i][j] == 0 && oldGrid[i][j] == 1) tempVector.push(new <int>[i,j, 0]);
					//if the brick is empty now and it was a part of current tetromino, it should be empty
					if(currentGrid[i][j] == 0 && oldGrid[i][j] == 2) tempVector.push(new <int>[i,j, 0]);
					//if the brick is a part of current tetromino, and it was empty, it should be a part of current tetromino
					if(currentGrid[i][j] == 2 && oldGrid[i][j] == 0) tempVector.push(new <int>[i,j, 2]);
				}
			}
			
			return tempVector;
		}
		
		
		private function cloneCurrentGrid():Vector.<Vector.<int>>
		{
			var i:int = 0;
			var tempGrid:Vector.<Vector.<int>> = new Vector.<Vector.<int>>(NUM_OF_ROWS, true);
			for (i; i < NUM_OF_ROWS; i++) 
			{
				tempGrid[i] = currentGrid[i].concat();
			}
			
			return tempGrid;
		}
	
	}
}